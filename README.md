# OSCP scripts

## usage

### oscp-mssql-error-based

```sh
# for dumping databases
$ python3 oscp-mssql-error-based <burp intercepted request file> -l <prefix> -r <suffix>

# for dumping tables
$ python3 oscp-mssql-error-based <burp intercepted request file> -l <prefix> -r <suffix> -d <database>[,database ...]

# for dumping columns
$ python3 oscp-mssql-error-based <burp intercepted request file> -l <prefix> -r <suffix> -d <database>[,database ...] -t <table>[,table ...]
```

### oscp-mssql-time-based

```sh
# for dumping databases
$ python3 oscp-mssql-time-based <burp intercepted request file> -l <prefix> -r <suffix>

# for dumping tables
$ python3 oscp-mssql-time-based <burp intercepted request file> -l <prefix> -r <suffix> -d <database>[,database ...]

# for dumping columns
$ python3 oscp-mssql-time-based <burp intercepted request file> -l <prefix> -r <suffix> -d <database>[,database ...] -t <table>[,table ...]
```
